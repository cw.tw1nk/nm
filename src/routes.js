import Home from './components/Home.vue';
import Lab8 from './components/Lab8.vue';
import Lab9 from './components/Lab9.vue';
import Lab10 from './components/Lab10.vue';
import Lab11 from './components/Lab11.vue';
import CourseWork from './components/CourseWork.vue';
const routes = [
    { path: '/', component: Home },
    { path: '/lab8', component: Lab8 },
    { path: '/lab9', component: Lab9 },
    { path: '/lab10', component: Lab10 },
    { path: '/lab11', component: Lab11 },
    { path: '/course-work', component: CourseWork },
];

export default routes;
