import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import routes from './routes'
import VueKatex from 'vue-katex'
import 'katex/dist/katex.min.css'
import VueInputAutowidth from 'vue-input-autowidth'
import Vuelidate from 'vuelidate'
Vue.config.productionTip = false;

Vue.use(VueKatex);
Vue.use(VueRouter);
Vue.use(VueInputAutowidth);
Vue.use(Vuelidate);
const router = new VueRouter({routes});
new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
